import java.util.Scanner;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		String[] firstIn = in.readLine().split(" ");
		int jmlBaris = Integer.parseInt(firstIn[0]);
		int jmlKolom = Integer.parseInt(firstIn[1]);
		
		int[][] papan = new int[jmlBaris][jmlKolom];
		for (int i = 0; i < jmlBaris; i++)
		{
			String[] baris = in.readLine().split("");
			for (int j = 0; j < jmlKolom; j++) 
			{
				papan[i][j] = Integer.parseInt(baris[j]);
			}
		}
		
		setPapan(papan, jmlBaris, jmlKolom);
	}
	
	/**
	 * Method untuk mengatur kondisi papan ketika terdapat 1 atau lebih baris yang terisi penuh
	 * @param papan - array tempat setiap kotak
	 * @param jmlBaris - jumlah baris dari array papan
	 * @param jmlKolom - jumlah kolom dari array papan
	 */
	public static void setPapan(int[][] papan, int jmlBaris, int jmlKolom)
	{
		ArrayList<Integer> barisPenuh = new ArrayList<Integer>();
		for (int i = 0; i < jmlBaris; i++)
		{
			int count = 0;
			for (int j = 0; j < jmlKolom; j++)
			{
				if (papan[i][j] == 1) {
					count++;
				}
				else {
					count = 0;
					break;
				}
			}
			
			if (count == jmlKolom) {
				barisPenuh.add(i);
			}
			
		}
		runtuhkan(papan, barisPenuh, jmlBaris, jmlKolom);
	}
	
	/**
	 * Method untuk menghapus baris yang terisi penuh dan meruntuhkan baris-baris yang ada di atasnya.
	 * @param papan - array tempat setiap kotak
	 * @param barisPenuh - ArrayList yang terdiri atas baris-baris yang terisi penuh
	 * @param jmlKolom - jumlah kolom dari array papan
	 */
	public static void runtuhkan(int[][] papan, ArrayList<Integer> barisPenuh, int jmlBaris, int jmlKolom)
	{
		//blok kode untuk menghapus isi kotak dari setiap baris yang penuh
		for (int i = 0; i < barisPenuh.size(); i++) 
		{
			for (int j = 0; j < jmlKolom; j++)
			{
				papan[barisPenuh.get(i)][j] = 0;
			}
		}
		
		
		
		
		//blok kode untuk test output setelah dihapus baris penuhnya
		for (int i = 0; i < jmlBaris; i++) {
			String out = "";
			for (int j = 0; j < jmlKolom; j++) {
				out += papan[i][j];
			}
			System.out.println(out);
		}
		
		System.out.println("-----------------------------------");
		
		/*
		 * Blok kode untuk meruntuhkan segala sesuatu yang ada di atas baris yg sudah dikosongkan.
		 * batas peruntuhan hanya sampai pada baris terbawah yang sudah dikosongkan.
		 */
		
		
		
		// blok kode untuk menentukan batas bawah tiap kolom
		int[] batasBawahKolom = new int[jmlKolom];
		
		for (int i = 0; i < jmlKolom; i++)
		{
			for (int j = barisPenuh.get(barisPenuh.size()-1); j < jmlBaris; j++)
			{
				if (papan[j][i] == 0) {
					batasBawahKolom[i] = j;
				}
				else if (papan[j][i] == 1) {
					break;
				}
			}
		}
		
		
		//blok kode untuk menghitung jumlah kotak yang TERISI pada SATU kolom dari baris penuh terbawah + 1 hingga baris terata
		int[] jmlIsi = new int[jmlKolom];
		for (int i = 0; i < jmlKolom; i++)
		{
			for (int j = 0; j <= batasBawahKolom[i]; j++)
			{
				if (papan[j][i] == 1) {
					jmlIsi[i]++;
				}
			}
		}
		
		//blok kode untuk meruntuhkan
		for (int i = 0; i < jmlKolom; i++) //iterasi setiap kolom
		{
			boolean isRuntuh = false;
			while (isRuntuh ==  false) 
			{
				int count = 0;
				
				//mengisi kotak pada kolom i baris j yang kosong dengan baris di atasnya
				for (int j = batasBawahKolom[i]; j > 0; j--)
				{
					if (papan[j][i] == 0) {
						papan[j][i] = papan[j-1][i];
						papan[j-1][i] = 0;
					}
				}
				
				//menghitung jumlah kotak terisi atau yang runtuh tiap kolom
				for (int j = batasBawahKolom[i]; j > batasBawahKolom[i] - jmlIsi[i]; j--)
				{
					if (papan[j][i] == 1)
						count++;
				}
				
				//mengecek apakah sudah runtuh hingga batas terbawah tiap kolom
				if (count == jmlIsi[i]) {
					isRuntuh = true;
				}
			}
		}
		
		for (int i = 0; i < jmlBaris; i++) {
			String out = "";
			for (int j = 0; j < jmlKolom; j++) {
				out += papan[i][j];
			}
			System.out.println(out);
		}
		
		
		
	}
	
}
